# Configure a [Modular.im](https://modular.im/) homeserver with custom domain if you do not have a web server

If you already have a website on `https://yourdomain.com`, this guide does not apply to you.

This guide will show step by step how to set up a [Modular.im](https://modular.im/) homeserver with a custom domain so that your Matrix username can appear as `@example:yourdomain.com` instead of `@example:yourserver.modular.im`

## Obtain a domain

Obtain a domain from a domain registrar. If you do not know any, I recommend [Hover](https://www.hover.com/)

## Register your free GitLab account

Click `Sign in/Register` in the top right corner  
![](pictures/01.png)

Click  `Register now`  
![](pictures/02.png)

Fill in your information and click `Register`  
![](pictures/03.png)

Fill in as applicable  
![](pictures/04.png)

Confirm your email by clicking `Confirm your account` in the email you received  
![](pictures/05.png)

You do not need an SSH key, so this can safely be ignored. Click `Don't show again`  
![](pictures/06.png)

# Create your GitLab page

Go back to this guide and click `Fork`  
![](pictures/07.png)

Click this  
![](pictures/08.png)

Click `Settings`, then `Pages`  
![](pictures/09.png)

Ensure `Force HTTPS (Requires valid certificates)` is checked, then click `New Domain`  
![](pictures/10.png)

Fill in your domain and ensure `Automatic certificate management using Let's Encrypt` is enabled  
![](pictures/11.png)

Now you need to create two DNS records with your domain provider. The first one is the `DNS` and the second one is the `Verification status`. You will need this information in a bit  
![](pictures/12.png)

## Create DNS record for your GitLab page

Go to your domain registrar. This guide is based on the registrar [Hover](https://www.hover.com/), if you are using another registrar please refer to their documentation.  
Click edit on the domain you want to use, then `EDIT DNS`  
![](pictures/13.png)

Remove any Type A entries where the Host is `blank`, * or @. Then click `ADD RECORD`  
![](pictures/14.png)

Change `Type` to `CNAME`,  
enter `@` in the `HOSTNAME` field,  
enter the last part of the `DNS` field from earlier in the `TARGET NAME` field,  
then click `ADD RECORD`  
![](pictures/15.png)

CLICK `ADD RECORD` again, this time change `Type` to `TXT`,  
enter the first part of the `Verification code` field from earlier in the `HOSTNAME` field, 
enter the last part of the `Verification code` field from earlier in the `TARGET NAME` field,  
then click `ADD RECORD`  
![](pictures/16.png)

Go back to GitLab. The `Verification status` is `Unverified`. Click the `Retry verification` button  
![](pictures/17.png)

If you added the TXT record correctly, this should change to `Verified`. Click `Save Changes`  
![](pictures/18.png)

It can take a while to obtain the certificate. Wait until this is complete  

If you get this error message, follow the instructions in the email from GitLab. Give it some time and try again  
![](pictures/19.png)

Please note that Modular Support will not help with problems with GitLab or your domains.

## Set up your [Modular.im](https://modular.im/) homeserver

Go to [Modular.im](https://modular.im/). Click `Sign up or Sign in`, enter your email address and click `Continue`  
![](pictures/20.png)

Click the link in the email you received, read our `Terms of Service`, `Privacy Policy` and `Cookie Policy`. Then click `I Agree`  
![](pictures/21.png)

Click `Please add your name now.`  
![](pictures/22.png)

Fill in your Name, click `Show password form`, enter and confirm your password. Then click `Save Changes`  
![](pictures/23.png)

Click `Billing address and payment method`  
![](pictures/24.png)

Fill in your information and click `Submit`  
![](pictures/25.png)

Click `Your Account`, then `Manage Servers`  
![](pictures/26.png)

Select the homeserver size you want, then click `Continue`  
![](pictures/27.png)

Enter a hostname, then click `Check`  
![](pictures/28.png)

COnfigure your host like you want it, then click `Custom DNS`  
![](pictures/29.png)

Enter your domain in the field `Custom Homeserver domain`. Then copy the text in the first grey box  
![](pictures/30.png)

Go back to GitLab, click `Repository`, then `Files`  
![](pictures/31.png)

Click the `public` folder  
![](pictures/32.png)

Click the `.well-known/matrix` folder  
![](pictures/33.png)

Click the `server` file
![](pictures/35.png)

Click `Edit`  
![](pictures/36.png)

Replace the text with the text from Modular. Then click `Commit changes`  
![](pictures/37.png)

Click the `matrix` folder  
![](pictures/38.png)

Repeat the last few steps for the text in the second grey box back on Modular. This time, click the `client` file instead of `server`  
![](pictures/39.png)

Go back to Modular and click `Check again` in both the orange boxes. If everything is working as it should they will turn green.
Note that this might take a minute to become available.  
![](pictures/40.png)

Enter `riot.yourdomain.com` in the `Custom Client domain` box. You can replace `riot` with anything you want. Copy the string under `CNAME`  
![](pictures/41.png)

Go back to your DNS provider and click `ADD A RECORD`  
![](pictures/42.png)

Change type to `CNAME`,  
enter `riot` in the `HOSTNAME` field, (replace with whatever you chose two steps ago),  
paste the string you copied two steps ago in the `TARGET NAME` field,
and click `ADD RECORD`  
![](pictures/43.png)

Go back to Modular, click `Check again` in the orange box. If you configured this DNS record correctly this box will turn green.  
Please note that it might take a while for these settings to become active.  
Click `Next`  
![](pictures/44.png)

Select your `Subscription period` and confirm your payment details. Then click `Purchase`  
![](pictures/45.png)

Give us a few minutes to create your server  
![](pictures/46.png)

